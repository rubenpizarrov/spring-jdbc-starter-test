package com.rpizarro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class BasicdbtestoraApplication implements CommandLineRunner {
	
	@Autowired
	private JdbcTemplate jdbcTemplate; 
	
	public static void main(String[] args) {
		SpringApplication.run(BasicdbtestoraApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String sql = "insert into cliente values (?,?,?,?)";
		
		
		int resultado = jdbcTemplate.update(sql, "4-41", "Jose", "Perez", 2500);
		
		if (resultado > 0) {
			System.out.println("Un nuevo cliente ha sido insertado");
		}
		
	}

}
