### Esto es una aplicación básica para probar la conexión a la base de datos Oracle 11g en particular


1 - Creación de Usuarios en Oracle11g básica:

```sql
CREATE USER MIUSUARIO IDENTIFIED BY MIPASSWORD 
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP;


GRANT "RESOURCE" TO MIUSUARIO;
GRANT "CONNECT" TO MIUSUARIO;
GRANT CREATE TABLE, CREATE SEQUENCE TO MIUSUARIO;
```

2- Crear el Proyecto spring-initializr con las siguientes dependencias 

- Spring Web
- JDBC-API
- Oracle Driver
- Spring Devtools (opcional)

Para referencias revisar el POM de este repositorio

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
		<!--Para trabajar con JSP de lo contrario no renderiza la vista-->
		<dependency>
			<groupId>org.apache.tomcat.embed</groupId>
			<artifactId>tomcat-embed-jasper</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.oracle.database.jdbc</groupId>
			<artifactId>ojdbc8</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
```
3 - Configurar el Datasource en el application.properties /main/resources/application.properties con los parametros, es importante
que no queden espacios en blanco despues de los valores 


```
para Oracle 18c --jdbc:oracle:thin:@//localhost:1521/XE
para Oracle 18c --jdbc:oracle:thin:@//localhost:1521/XEPDB1 
spring.datasource.url=jdbc:oracle:thin:@localhost:1521:orcl
spring.datasource.username=MIUSUARIO
spring.datasource.password=MIPASSWORD
spring.datasource.driver-class-name=oracle.jdbc.OracleDriver
```

4 - En la clase main se ejecutará como linea de commandos el insert

```java

//Nota: Para que aparezcan comando por consola se implementa la interfaz CommandLineRunner
//y se sobreescribe el metodo de la interfaz más abajo 
@SpringBootApplication
public class BasicdbtestoraApplication implements CommandLineRunner {
	
    //Esta anotación es necesaria para que mapee la Clase JdbcTemplate
	@Autowired
	private JdbcTemplate jdbcTemplate; 
	
	public static void main(String[] args) {
		SpringApplication.run(BasicdbtestoraApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
        
        //Se crea la Sentencia SQL
		String sql = "insert into cliente values (?,?,?,?)";
		
        //Devolverá un número de filas afectadas
		int resultado = jdbcTemplate.update(sql, "4-41", "Jose", "Perez", 2500);
		
        //Si es mayor a 0 significa que insertó y se verá en la consola de salida
		if (resultado > 0) {
			System.out.println("Un nuevo cliente ha sido insertado");
		}
		
	}

}
```

Si todo ha ido bien debería haber insertado el usuario, ojo, cada vez que ejecuten el proyecto intentará insertar inmediatamente
por lo que dará errores al intentar sobreescribir un valor de Primary Key, deben cambiar los datos cada vez. 
